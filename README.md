# Docker Image for Build Android 

[![pipeline status](https://gitlab.com/lucianradu/android-ci/badges/master/pipeline.svg)](https://gitlab.com/lucianradu/android-ci/commits/master)

GitLab CI image for building Android apps

## Example `.gitlab-ci.yml` file
Please add your required sdk package with `sdkmanager` command, see example below

```yml
image: registry.gitlab.com/radulucian/android-ci:master

before_script:
    - export GRADLE_USER_HOME=`pwd`/.gradle
    - chmod +x ./gradlew
    - sdkmanager "platforms;android-22"         # Specify compileSdkVersion Depends on your android project
    - sdkmanager "build-tools;25.0.3"           # Specify build tool to install, depends on your android project
    - sdkmanager "extras;android;m2repository"  # You can add sdkmanager command to install another package here
    - sdkmanager "extras;m2repository;com;android;support;constraint;constraint-layout;1.0.2" # If you need constraint-layout support
   

cache:
  key: "$CI_COMMIT_REF_NAME"
  paths:
     - .gradle/

stages:
  - test
  - build

test:
  stage: test
  script:
     - ./gradlew check

build:
  stage: build
  script:
     - ./gradlew assemble

```
